package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Erstellt ein Fenster in der die Level ausgew�hlt werden k�nnen.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user,
			String source) {
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());
		//LS03-02-2 (8.) Das Fenster maximiert starten.
		leveldesignWindow.setExtendedState(leveldesignWindow.MAXIMIZED_BOTH);

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});

		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSpielen_Clicked(alienDefenceController, user);
			}
		});
		
		JButton btnHighscoreAuswahl = new JButton("Ausw�hlen");
		btnHighscoreAuswahl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHighscoreAuswahl_Clicked(alienDefenceController);
			}
		});
		pnlButtons.add(btnHighscoreAuswahl);
		pnlButtons.add(btnSpielen);
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		// die Gr�ne Farbe als Hex Code von dem Main Men� extrahiert (#7CFC00)
		lblLevelauswahl.setForeground(Color.decode("#7CFC00"));
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setOpaque(true);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setBackground(Color.BLACK);
		add(spnLevels, BorderLayout.CENTER);

		// Die Tabelle nach den Designvorgaben anpassen in LS03-02-2
		tblLevels = new JTable();
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setGridColor(Color.BLACK);
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();

		// Buttons nach �bergebenen String source auswerten und entsprechend ausblenden. (LS03-02-2 + Zusatz)
		if (source.equals("Testen")) {
			btnNewLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnHighscoreAuswahl.setVisible(false);
		} else if (source.equals("Leveleditor")) {
			btnSpielen.setVisible(false);
			btnHighscoreAuswahl.setVisible(false);
		} else if (source.equals("Highscore")) {
			btnHighscoreAuswahl.setVisible(true);
			btnSpielen.setVisible(false);
			btnNewLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}

	/**
	 * Methode welche es das ausgew�hlte Level startet f�r den Nutzer.
	 * 
	 * @param alienDefenceController Der Controller, welcher n�tig ist um ein Spiel
	 *                               zu starten.
	 * @param user                   Das User Objekt, welches n�tig ist um ein Spiel
	 *                               zu starten.
	 */
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, User user) {
		// Level Id aus der Tabelle auslesen und in der Variable level_id speichern.
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		// Das ausgew�hlte Level aus der Persistenz laden.
		Level level = alienDefenceController.getLevelController().readLevel(level_id);
		// Gameprozess starten
		Thread t = new Thread("GameThread") {

			@Override
			public void run() {
				// Hier wird das neue Spiel aufgerufen
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();
			}

		};
		// Nun den Prozess starten
		t.start();
		// Das Levelauswahlfenster schlie�en.
		this.leveldesignWindow.dispose();
	}
	
	public void btnHighscoreAuswahl_Clicked(AlienDefenceController alienDefenceController) {
		// Prinzipiell derselbe Vorgang wie bei der Methode btnSpielen_Clicked, allerdings wird hier
		// ein Highscorefenster ge�ffnet.
		int level_id = Integer
				.parseInt((String)tblLevels.getModel().getValueAt(tblLevels.getSelectedRow(), 0));
		Level level = alienDefenceController.getLevelController().readLevel(level_id);
		new Highscore(alienDefenceController.getAttemptController(), level);
		// Levelauswahl schlie�en
		this.leveldesignWindow.dispose();
	}
}
