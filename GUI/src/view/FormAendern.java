package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;

public class FormAendern extends JFrame {

	private JPanel contentPane;
	private JTextField txtEingabefeld;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormAendern frame = new FormAendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormAendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 383, 585);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTop = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblTop.setHorizontalAlignment(SwingConstants.CENTER);
		lblTop.setBounds(57, 59, 252, 14);
		contentPane.add(lblTop);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(12, 104, 216, 14);
		contentPane.add(lblAufgabe1);
		
		JButton btnHintergrundRot = new JButton("Rot");
		btnHintergrundRot.setFont(new Font("Dialog", Font.BOLD, 11));
		btnHintergrundRot.setBounds(8, 119, 116, 26);
		contentPane.add(btnHintergrundRot);
		
		JButton btnHintergrundGrn = new JButton("Gr\u00FCn");
		btnHintergrundGrn.setFont(new Font("Dialog", Font.BOLD, 11));
		btnHintergrundGrn.setBounds(136, 119, 116, 26);
		contentPane.add(btnHintergrundGrn);
		
		JButton btnHintergrundBlau = new JButton("Blau");
		btnHintergrundBlau.setFont(new Font("Dialog", Font.BOLD, 11));
		btnHintergrundBlau.setBounds(264, 119, 116, 26);
		contentPane.add(btnHintergrundBlau);
		
		JButton btnHintergrundGelb = new JButton("Gelb");
		btnHintergrundGelb.setFont(new Font("Dialog", Font.BOLD, 11));
		btnHintergrundGelb.setBounds(8, 156, 116, 26);
		contentPane.add(btnHintergrundGelb);
		
		JButton btnHintergrundStandardfarbe = new JButton("Standardfarbe");
		btnHintergrundStandardfarbe.setFont(new Font("Dialog", Font.BOLD, 11));
		btnHintergrundStandardfarbe.setBounds(136, 156, 116, 26);
		contentPane.add(btnHintergrundStandardfarbe);
		
		JButton btnHintergrundFarbeWaehlen = new JButton("Farbe w\u00E4hlen");
		btnHintergrundFarbeWaehlen.setFont(new Font("Dialog", Font.BOLD, 11));
		btnHintergrundFarbeWaehlen.setBounds(264, 157, 116, 26);
		contentPane.add(btnHintergrundFarbeWaehlen);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(8, 195, 216, 14);
		contentPane.add(lblAufgabe2);
		
		txtEingabefeld = new JTextField();
		txtEingabefeld.setText("Hier bitte Text eingeben");
		txtEingabefeld.setBounds(8, 244, 355, 20);
		contentPane.add(txtEingabefeld);
		txtEingabefeld.setColumns(10);
		
		JButton btnArial = new JButton("Arial");
		btnArial.setFont(new Font("Dialog", Font.BOLD, 11));
		btnArial.setBounds(8, 211, 116, 26);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.setFont(new Font("Dialog", Font.BOLD, 11));
		btnComicSansMs.setBounds(136, 211, 116, 26);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.setFont(new Font("Dialog", Font.BOLD, 11));
		btnCourierNew.setBounds(265, 211, 116, 26);
		contentPane.add(btnCourierNew);
		
		JButton btnArial_1 = new JButton("Ins Label schreiben");
		btnArial_1.setFont(new Font("Dialog", Font.BOLD, 11));
		btnArial_1.setBounds(8, 273, 188, 26);
		contentPane.add(btnArial_1);
		
		JButton btnArial_1_1 = new JButton("Text im Label l\u00F6schen");
		btnArial_1_1.setFont(new Font("Dialog", Font.BOLD, 11));
		btnArial_1_1.setBounds(201, 273, 188, 26);
		contentPane.add(btnArial_1_1);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(7, 311, 216, 14);
		contentPane.add(lblAufgabe3);
		
		JButton btnArial_2 = new JButton("Arial");
		btnArial_2.setFont(new Font("Dialog", Font.BOLD, 11));
		btnArial_2.setBounds(7, 327, 116, 26);
		contentPane.add(btnArial_2);
		
		JButton btnComicSansMs_1 = new JButton("Comic Sans MS");
		btnComicSansMs_1.setFont(new Font("Dialog", Font.BOLD, 11));
		btnComicSansMs_1.setBounds(136, 327, 116, 26);
		contentPane.add(btnComicSansMs_1);
		
		JButton btnCourierNew_1 = new JButton("Courier New");
		btnCourierNew_1.setFont(new Font("Dialog", Font.BOLD, 11));
		btnCourierNew_1.setBounds(264, 327, 116, 26);
		contentPane.add(btnCourierNew_1);
	}
}
